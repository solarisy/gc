<%@ page contentType="text/html;charset=UTF-8" %>

<pre>
(root)
 |
 +- class
 |   |
 |   +- tableName <font color=green>// 表名如：t_product</font>
 |   |
 |   +- name <font color=green>// 类名如：Product</font>
 |   |
 |   +- comment <font color=green>// 类的注释(数据库中表的注释)</font>
 |   |
 |   +- fields <font color=green>// 这是一个List对象，可以在ftl中使用&lt;#list class.fields as field&gt;...&lt;/#list&gt;遍历</font>
 |       |
 |       +- (1st)
 |       |   |
 |       |   +- columnName <font color=green>// 数据库字段名称如：product_name,product_address</font>
 |       |   |
 |       |   +- name <font color=green>// 字段名称，出除一个字母外，每个单词首字母都大写如：productName,productAddress</font>
 |       |   |
 |       |   +- nameForPascal <font color=green>// 字段名称，每个单子首字母都大写如：ProductName,ProductAddress</font>
 |       |   |
 |       |   +- type <font color=green>// Java 数据类型如：“String”，“Long”,“Date”...</font>
 |       |   |
 |       |   +- comment <font color=green>// 字段注释(表中的字段注释)</font>
 |       |
 |       +- (2st)
 |       .   |
 |       .   +- ... ...
 |       .   
 |       +- (Nst)
 |
 +- userData <font color=green>// 用户自定义数据模型，这是一个Map对象</font>
     |
     +- (...) = "..."
</pre>