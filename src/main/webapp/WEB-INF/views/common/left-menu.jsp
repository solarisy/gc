<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.ExcessiveAttemptsException"%>
<%@ page import="org.apache.shiro.authc.IncorrectCredentialsException"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
			

<ul class="nav nav-tabs nav-stacked main-menu">
	<li class="nav-header hidden-tablet">==我的方案==</li>
	<c:forEach items="${projects}" var="project" varStatus="status" >
		<li><a class="ajax-link" href="${ctx}/project/myProject/${project.id}"  title="${project.description}">&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-home"></i><span class="hidden-tablet">${project.name}</span></a></li>
	</c:forEach>
	<li class="nav-header hidden-tablet">==系统配置==</li>
	<li><a class="ajax-link" href="${ctx}/project" data-toggle="tooltip" title="方案的CRUD">方案管理</a></li>
</ul>
		