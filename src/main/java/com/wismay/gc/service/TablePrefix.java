package com.wismay.gc.service;

import java.util.ArrayList;
import java.util.List;
/**
 *  需要移除的字符串(生产实体类时不需要的表前缀)
 * @author Administrator
 *
 */
public class TablePrefix {
	//表前缀
	public static List<String> getTablePrefixStr() {
		List<String> removeStr = new ArrayList<String>();
		removeStr.add("t_");
		removeStr.add("p_");
		removeStr.add("d_");
		removeStr.add("g_");
		removeStr.add("wx_");
		removeStr.add("f_");
		return removeStr;
	}
}
