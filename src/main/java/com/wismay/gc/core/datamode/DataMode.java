package com.wismay.gc.core.datamode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据模型.Freemark根据该信息填充源码模板
 * 
 * @author Petter
 * 
 */
public class DataMode {
	private List<Class> classes = new ArrayList<Class>();// 需要生成类的信息
	private Map<String, Object> userData = new HashMap<String, Object>();// 用户自定义数据

	public DataMode() {
		super();
	}

	public DataMode(List<Class> classes, Map<String, Object> userData) {
		super();
		this.classes = classes;
		this.userData = userData;
	}

	public List<Class> getClasses() {
		return classes;
	}

	public void setClasses(List<Class> classes) {
		this.classes = classes;
	}

	public Map<String, Object> getUserData() {
		return userData;
	}

	public void setUserData(Map<String, Object> userData) {
		this.userData = userData;
	}

}
