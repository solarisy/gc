package com.wismay.gc.core.datamode;

import java.util.List;

/**
 * 类信息。Freemark根据该信息生成类及其属性。
 * 
 * @author Petter
 * 
 */
public class Class {
	private String tableName;// 表名
	private String name;// 类名
	private String comment;// 类的注释
	private List<Field> fields;// 类中的字段信息

	public Class() {
		super();
	}

	public Class(String name, String comment) {
		super();
		this.name = name;
		this.comment = comment;
	}

	public Class(String name, String comment, List<Field> fields) {
		super();
		this.name = name;
		this.comment = comment;
		this.fields = fields;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<Field> getFields() {
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

}
