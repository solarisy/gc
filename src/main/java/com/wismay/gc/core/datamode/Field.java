package com.wismay.gc.core.datamode;

public class Field {
	private String columnName;// 数据库字段名称
	private String name;// 字段名称，出除一个字母外，每个单词首字母都大写
	private String nameForPascal;//字段名称，每个单子首字母都大写
	private String type;// Java 数据类型
	private String comment;// 字段注释

	public Field() {
		super();
	}

	public Field(String name, String type, String comment) {
		super();
		this.name = name;
		this.type = type;
		this.comment = comment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getNameForPascal() {
		return nameForPascal;
	}

	public void setNameForPascal(String nameForPascal) {
		this.nameForPascal = nameForPascal;
	}
	

}
