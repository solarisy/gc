package com.wismay.gc.core.transform.impl;

import java.util.ArrayList;
import java.util.List;

import com.wismay.gc.core.datamode.Class;
import com.wismay.gc.core.datamode.DataMode;
import com.wismay.gc.core.datamode.Field;
import com.wismay.gc.core.metamode.Column;
import com.wismay.gc.core.metamode.MetaMode;
import com.wismay.gc.core.metamode.Table;
import com.wismay.gc.core.transform.JavaNameing;
import com.wismay.gc.core.transform.NameType;
import com.wismay.gc.core.transform.Transformer;

public class JavaTransformer implements Transformer {
	
	/**
	 * 
	 * @param metaMode
	 * @param removeStrs 需要移除的表名前缀。一般数据库的表名都会有前缀，如（"t_"、"bas_"等等）。
	 * @return
	 */
	public static DataMode transform(MetaMode metaMode,List<String> removeStrs){
		DataMode dataMode=new DataMode();
		
		List<Class> classes=new ArrayList<Class>();
		for(Table table:metaMode.getTables()){
			Class c=new Class();
			c.setTableName(table.getName());
			c.setName(JavaNameing.dbNameToObjName(table.getName(), NameType.PASCAL, removeStrs));
			c.setComment(table.getComment());
			
			List<Field> fields=new ArrayList<Field>();
			for(Column col:table.getColumns()){
				Field field=new Field();
				field.setColumnName(col.getColName());
				field.setName(JavaNameing.dbNameToObjName(col.getColName(), NameType.CAMEL, null));
				field.setNameForPascal(JavaNameing.dbNameToObjName(col.getColName(), NameType.PASCAL, null));
				field.setType(JavaNameing.transformType(col.getType()));
				field.setComment(col.getComment());
				fields.add(field);
			}
			c.setFields(fields);
			
			classes.add(c);
		}
		
		dataMode.setClasses(classes);
		return dataMode;
	}
}
