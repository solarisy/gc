package com.wismay.gc.core.transform;

/**
 * 
 * 命名规则
 * 
 */
public enum NameType {
	/**
	 * 首字母大写UserName
	 */
	PASCAL,
	/**
	 * 驼峰命名（除了第一个单词，其他单词首字母都大写）userName
	 */
	CAMEL
}