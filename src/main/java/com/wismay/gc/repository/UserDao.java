package com.wismay.gc.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.wismay.gc.entity.User;

public interface UserDao extends PagingAndSortingRepository<User, Long> {
	User findByLoginName(String loginName);
}
